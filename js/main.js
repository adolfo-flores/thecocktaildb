const COCTELES_POR_PAGINA = 15;

async function mostrarCocktails(category, page) {
    try {
        const response = await axios.get(`https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=${category}`);
        const data = response.data;

        if (data.drinks && data.drinks.length > 0) {
            const resultadoDiv = document.getElementById('resultado');
            resultadoDiv.innerHTML = ''; 

            data.drinks.forEach(cocktail => {
                const enlace = document.createElement('div');
                enlace.style.width = '300px';
                enlace.style.height = '300px';
                enlace.style.overflow = 'hidden';
                enlace.style.margin = '10px';
                enlace.style.display = 'inline-block';

                mostrarDetalleCocktail(enlace, cocktail);

                resultadoDiv.appendChild(enlace);
            });
        } else {
            alert(`No se encontraron cócteles ${category === 'Alcoholic' ? 'alcohólicos' : 'no alcohólicos'}.`);
        }
    } catch (error) {
        console.error('Error al hacer la solicitud:', error);
    }
}

function mostrarDetalleCocktail(container, cocktail) {
    const nombreCocktail = document.createElement('h2');
    nombreCocktail.textContent = cocktail.strDrink;
    container.appendChild(nombreCocktail);

    const imagenCocktail = document.createElement('img');
    imagenCocktail.src = cocktail.strDrinkThumb;
    imagenCocktail.alt = cocktail.strDrink;
    imagenCocktail.style.width = '100%'; 
    container.appendChild(imagenCocktail);

    container.onclick = function () {
        mostrarDetalleCocktail(container, cocktail);
    };
}


function limpiarResultados() {
    const resultadoDiv = document.getElementById('resultado');
    resultadoDiv.innerHTML = '';
}

